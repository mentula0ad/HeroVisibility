# About HeroVisibility

HeroVisibility is a [0 A.D.](https://play0ad.com) mod for a better localization of heroes in the fight. The feature provided by this mod is borrowed from the [Hyrule Conquest mod](https://wildfiregames.com/forum/topic/22638-hyrule-conquest) and included in the [boonGUI mod](https://github.com/LangLangBart/boonGUI) (and possibly other mods). So, if you are already using any of the above mentioned mods, you don't need the HeroVisibility mod. However, you might want to take advantage of the functionality provided by the HeroVisibility mod without enabling other mods; then HeroVisibility is right for you.

![](img/bitmap.png "Basic illustration of the HeroVisibility functionality")

# Installation

1. Download the source code of the [latest release](https://gitlab.com/mentula0ad/HeroVisibility/-/releases) and extract it.
2. Open the extracted folder and locate the sub-folder named `HeroVisibility`.
3. Copy the `HeroVisibility` folder into the `mods` folder, typically located at:
   * Linux: `~/.local/share/0ad/mods/`
   * macOS: `~/Library/Application\ Support/0ad/mods/`
   * Windows: `~\Documents\My Games\0ad\mods\`
4. Launch 0 A.D. and open the `Settings > Mod Selection` menu.
5. Select the HeroVisibility mod, `Enable` it and `Save Configuration`.
